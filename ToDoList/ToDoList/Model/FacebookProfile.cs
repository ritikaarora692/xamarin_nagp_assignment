﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ToDoList.Model
{
    class FacebookProfile
    {
        public string Name { get; set; }
        public Picture Picture { get; set; }
        public string Id { get; set; }
    }

    public class Picture
    {
        public Data Data { get; set; }
    }

    public class Data
    {
        public bool IsSilhouette { get; set; }
        public string Url { get; set; }
    }

    public class Device
    {
        public string Os { get; set; }
    }
}
