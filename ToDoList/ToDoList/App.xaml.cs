﻿using Plugin.Geolocator.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoList.services;
using ToDoList.View;
using Xamarin.Forms;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace ToDoList
{
    public partial class App : Application
    {
        static MyDataBase database;
        public static int CurrentQuest = (new Random()).Next(1,10);
        public static int Points = 0;
        public static string Username = "";

        public static int TOTAL_QUESTION = CurrentQuest+4;
        public static int QUESTIONS_COUNT = CurrentQuest + 4;
        public static string DefaultImageId = "default_image";
        public static string ImageIdToSave = null;

        public App()
        {
            InitializeComponent();
          
            MainPage = new ToDoList.View.LoginPage();
        }

        public static MyDataBase Database
        {
            get
            {
                if (database == null)
                {
                    database = new MyDataBase(DependencyService.Get<IFileHelper>().GetLocalFilePath("TodoSQLite.db3"));
                }
                return database;
            }
        }

        public int ResumeAtToDoId { get; set; }

        protected override void OnStart()
        {
            AppCenter.Start("android=63c7595d-c115-45a9-a0ed-a252df7a7c0c;" +
                  "uwp={Your UWP App secret here};" +
                  "ios={Your iOS App secret here}",
                  typeof(Analytics), typeof(Crashes));
            AppCenter.Start("android=63c7595d-c115-45a9-a0ed-a252df7a7c0c;" + 
                "uwp={Your UWP App secret here};" + 
                "ios={Your iOS App secret here}", 
                typeof(Analytics), typeof(Crashes));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
