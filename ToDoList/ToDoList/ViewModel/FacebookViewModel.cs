﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using ToDoList.Model;
using ToDoList.services;

namespace ToDoList.ViewModel
{
    class FacebookViewModel : INotifyPropertyChanged
    {
        private FacebookProfile _facebookProfile;

        string token;

        public FacebookProfile FacebookProfile
        {
            get { return _facebookProfile; }
            set
            {
                _facebookProfile = value;
                App.Username = _facebookProfile.Name;

                OnPropertyChanged();
            }
        }

        public async Task SetFacebookUserProfileAsync(string tokens)
        {
            var facebookService = new FacebookService();

            token = App.Current.Properties["key"] as string;

           // FacebookProfile = await facebookService.GetFacebookProfileAsync(tokens);

            var facebookProfile = new FacebookProfile
            {
                Name = App.Username,
                Picture = new Picture
                {

                    Data = new Data
                    {
                        Url = "logo.png"
                    }

                }
            };
            FacebookProfile = facebookProfile;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

