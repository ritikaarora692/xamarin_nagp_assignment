﻿using System;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using ToDoList.Model;
using ToDoList.services;
using ToDoList.View;
using Xamarin.Forms;

namespace ToDoList.ViewModel
{
    public class LoginViewModel 
    {
        public Action DisplayInvalidLoginPrompt;
        public LoginPage loginPages;
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        private string email;
        public INavigation _navigation;
        private string _message;
        private bool _isLoggedIn;
        private FacebookUser _facebookUser;
        private IFacebookLogin _facebookService;

        public event PropertyChangedEventHandler PropertyChange;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChange?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public ICommand OnFacebookLoginSuccessCmd { get; }
        public ICommand OnFacebookLoginErrorCmd { get; }
        public ICommand OnFacebookLoginCancelCmd { get; }

        public Command FacebookLoginCommand { get; protected set; }
        public Command FacebookLogoutCommand { get; protected set; }

        LoginPage facebookPages;
        public string Email
        {
            get { return email; }
            set
            {
                email = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Email"));
            }
        }

        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
                OnPropertyChanged(nameof(Message));
            }
        }
        public bool IsLoggedIn
        {
            get
            {
                return _isLoggedIn;
            }
            set
            {
                _isLoggedIn = value;
                OnPropertyChanged(nameof(IsLoggedIn));
            }
        }
        public FacebookUser FacebookUser
        {
            get
            {
                return _facebookUser;
            }
            set
            {
                _facebookUser = value;
                OnPropertyChanged(nameof(FacebookUser));
            }
        }
        private string password;
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Password"));
            }
        }
        public ICommand SubmitCommand { protected set; get; }
        public LoginViewModel(INavigation navigation,LoginPage loginPage)
        {
            _navigation = navigation;
            loginPages = loginPage;
            SubmitCommand = new Command(OnSubmit);
            _navigation = navigation;


            _facebookService = DependencyService.Get<IFacebookLogin>();

            FacebookLoginCommand = new Command(FacebookLogin);
            FacebookLogoutCommand = new Command(FacebookLogout);

            facebookPages = loginPage;

           

        }
        async public void OnSubmit()
        {
            App.Username = email;
            if (email != "nagp@nagarro.com" || password != "nagp")
            {
                DisplayInvalidLoginPrompt();
            }
            else {
                await _navigation.PushModalAsync(new ProfilePage(""));
                }
        }

        void DisplayAlert(string title, string msg) =>
            (Application.Current as App).MainPage.DisplayAlert(title, msg, "OK");

        async void DisplayAlerts(string title, string msg) =>
         await _navigation.PushModalAsync(new ProfilePage(msg));
         //await _navigation.PushModalAsync(new SingleQuiz());

        private async void openProfile(string title, string msg)
        {

            await _navigation.PushAsync(new ProfilePage("test"));
        }

        private void FacebookLogin()
        {
            _facebookService?.Login(OnLoginCompleted);
        }
        private void FacebookLogout()
        {
            _facebookService?.Logout();
            IsLoggedIn = false;
        }

        private void OnLoginCompleted(FacebookUser facebookUser, Exception exception)
        {
            if (exception == null)
            {
                FacebookUser = facebookUser;
                IsLoggedIn = true;
                navigate();
            }
            else
            {
                App.Current.MainPage.DisplayAlert("Error", exception.Message, "OK");
            }
        }

        async public void navigate() {
            await _navigation.PushModalAsync(new ProfilePage(FacebookUser.Token));
        }

    }
}
