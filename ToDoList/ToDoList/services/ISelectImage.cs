﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoList.Model;

namespace ToDoList.services
{
    public interface ISelectImage
    {
        void LaunchGallery(FileFormats imageType, string imageId = null);
    }
}
