﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoList.DLL;
using ToDoList.Model;
using ToDoList.ViewModel;

namespace ToDoList.services
{

    public class QuestionService 
    {
        DatabaseHelper _databaseHelper;
        public QuestionService()
        {
            _databaseHelper = new DatabaseHelper();
        }

        public List<MyQuiz> GetAllQuestionData()
        {
            return _databaseHelper.GetAllQuestionData();
        }

        
        public MyQuiz GetQuestionData(int jobID)
        {
            return _databaseHelper.GetQuestionData(jobID);
        }

        

        public void InsertQuestion(MyQuiz contact)
        {
            _databaseHelper.InsertQuestion(contact);
        }

        public void InsertPoints(UserPoints userPoints)
        {
            _databaseHelper.InsertPoints(userPoints);
        }
        
    }
}
