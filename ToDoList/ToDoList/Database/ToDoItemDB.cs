﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net;
using Xamarin.Forms;
using ToDoList.ViewModel;

namespace ToDoList
{
    public class MyDataBase
    {
        readonly SQLiteAsyncConnection database;

        public MyDataBase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
        }
        

    }
}
