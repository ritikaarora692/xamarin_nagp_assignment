﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net;
using Xamarin.Forms;
using ToDoList.ViewModel;
using ToDoList.Model;

namespace ToDoList.DLL
{
    public class DatabaseHelper
    {

        static SQLiteConnection sqliteconnection;
        public const string DbFileName = "Contacts.db";

        public DatabaseHelper()
        {
            sqliteconnection = DependencyService.Get<ISQLite>().GetConnection();
            sqliteconnection.CreateTable<UserPoints>();
            sqliteconnection.CreateTable<MyQuiz>();

            InsertQuestion(new MyQuiz
            {
                Question = "Which of these class is superclass of String and StringBuffer class?",
                Answer1 = "java.util",
                Answer2 = "java.lang",
                Answer3 = "None of the above",
                CorrectAnswer = 2
            });

          
            InsertQuestion(new MyQuiz
            {
                Question = "Which of these operators can be used to concatenate two or more String objects?",
                Answer1 = "+",
                Answer2 = "+=",
                Answer3 = "=",
                CorrectAnswer = 1
            });

            InsertQuestion(new MyQuiz
            {
                Question = "What is the output of relational operators?",
                Answer1 = "Boolean",
                Answer2 = "Double",
                Answer3 = "Integer",
                CorrectAnswer = 1
            });

            InsertQuestion(new MyQuiz
            {
                Question = "Which right shift operator preserves the sign of the value?",
                Answer1 = ">>",
                Answer2 = ">>=",
                Answer3 = "None of The above",
                CorrectAnswer = 1
            });

            InsertQuestion(new MyQuiz
            {
                Question = "Which operator is used to invert all the digits in binary representation of a number?",
                Answer1 = "~",
                Answer2 = ">>>",
                Answer3 = "^",
                CorrectAnswer = 1
            });

            InsertQuestion(new MyQuiz
            {
                Question = "Decrement operator, –, decreases value of variable by what number?",
                Answer1 = "1",
                Answer2 = "2",
                Answer3 = "3",
                CorrectAnswer = 1
            });

            InsertQuestion(new MyQuiz
            {
                Question = "Which of the following can be operands of arithmetic operators?",
                Answer1 = "Numeric",
                Answer2 = "Character",
                Answer3 = "None of the above",
                CorrectAnswer = 1
            });

            InsertQuestion(new MyQuiz
            {
                Question = "Which of these interface must contain a unique element?",
                Answer1 = "Set",
                Answer2 = "List",
                Answer3 = "Array",
                CorrectAnswer = 1
            });

            InsertQuestion(new MyQuiz
            {
                Question = "Which of these interface handle sequences?",
                Answer1 = "Set",
                Answer2 = "List",
                Answer3 = "Comparator",
                CorrectAnswer = 2
            });

            InsertQuestion(new MyQuiz
            {
                Question = "Which of these methods deletes all the elements from invoking collection?",
                Answer1 = "clear()",
                Answer2 = "reset()",
                Answer3 = "delete()",
                CorrectAnswer = 1
            });

            InsertQuestion(new MyQuiz
            {
                Question = "Which of these classes is part of Java’s collection framework?",
                Answer1 = "Maps",
                Answer2 = "Array",
                Answer3 = "Queue",
                CorrectAnswer = 3
            });

            InsertQuestion(new MyQuiz
            {
                Question = "Which of these packages contain all the collection classes?",
                Answer1 = "java.lang",
                Answer2 = "java.util",
                Answer3 = "java.awt",
                CorrectAnswer = 2
            });

            InsertQuestion(new MyQuiz
            {
                Question = "Which of these operators is used to allocate memory to array variable in Java?",
                Answer1 = "malloc",
                Answer2 = "new",
                Answer3 = "calloc",
                CorrectAnswer = 2
            });
            InsertQuestion(new MyQuiz
            {
                Question = "Which of these constructors is used to create an empty String object?",
                Answer1 = "String()",
                Answer2 = "String(void)",
                Answer3 = "String(0)",
                CorrectAnswer = 1
            });

            InsertQuestion(new MyQuiz
            {
                Question = "Which of these method of class String is used to extract a single character from a String object?",
                Answer1 = "CHARAT()",
                Answer2 = "charat()",
                Answer3 = "charAt()",
                CorrectAnswer = 3
            });

        }
        public List<MyQuiz> GetAllQuestionData()
        {
            return (from data in sqliteconnection.Table<MyQuiz>()
                    select data).ToList();
        }
        
        public MyQuiz GetQuestionData(int id)
        {
            return sqliteconnection.Table<MyQuiz>().FirstOrDefault(t => t.CorrectAnswer == id);
        }

        
        public void InsertQuestion(MyQuiz question)
        {
            sqliteconnection.Insert(question);
        }

        public void InsertPoints(UserPoints userPoints)
        {
            sqliteconnection.Insert(userPoints);
        }

    }
}
