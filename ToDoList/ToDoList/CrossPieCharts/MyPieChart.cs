﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CrossPieCharts.FormsPlugin.Abstractions
{
    
    public class CrossPieChartSample
    {
        public ContentPage GetPageWithPieChart(int percentage, String username)
        {
            // The root page of your application
            var contentPage = new ContentPage
            {

                Content = new Grid
                {
                    Children =
                    {
                        new Grid // a trick to set the BackgroundColor of the ContentPage to white
                        {
                            BackgroundColor  = Color.FromHex("#b9a1ce")
                        },
                        new StackLayout
                        {
                            Children =
                            {
                                new Grid
                                {
                                    Children =
                                    {
                                        new Label
                                {
                                    XAlign = TextAlignment.End,
                                    YAlign= TextAlignment.Center,
                                    Text = "QUIZAPP",
                                    TextColor=Color.White,
                                    FontAttributes=FontAttributes.Bold,
                                    BackgroundColor=Color.FromHex("#563d7c"),
                                    FontSize=14,
                                    HeightRequest=20
                                },
                                        new Image
                                {
                                    Source="my_logo.jpg",
                                    HeightRequest=100,
                                    HorizontalOptions=LayoutOptions.Start,
                                    VerticalOptions=LayoutOptions.EndAndExpand ,
                                    
                                }
                                    }
                                },
                                new Grid
                                {
                                    Padding=30,
                                    Children =
                                    {
                                        new CrossPieChartView
                                        {
                                            Progress = percentage,
                                            ProgressColor = Color.FromHex("#563d7c"),

                                            ProgressBackgroundColor = Color.White,
                                            StrokeThickness = Device.OnPlatform(10, 20, 80),
                                            Radius = Device.OnPlatform(100, 180, 160),
                                            BackgroundColor = Color.FromHex("#b9a1ce")
                                        },
                                        new Label
                                        {
                                            Text = percentage+"%",
                                            Font = Font.BoldSystemFontOfSize(NamedSize.Large),
                                            FontSize = 40,
                                            VerticalOptions = LayoutOptions.Center,
                                            HorizontalOptions = LayoutOptions.Center,
                                            TextColor = Color.FromHex("#563d7c")
                                        }
                                    }
                                },
                                new Label
                                {
                                    Text = "Congratulations  "+username.Substring(0,username.LastIndexOf("@"))+"!",
                                    TextColor = Color.FromHex("#563d7c"),
                                    HorizontalOptions = LayoutOptions.Center,
                                    FontSize=18,
                                    FontFamily="Comic Sans"
                                },

                                 new Image
                                {
                                    Source="result_2.jpeg",
                                    VerticalOptions=LayoutOptions.FillAndExpand ,
                                    HorizontalOptions=LayoutOptions.FillAndExpand,
                                    HeightRequest=300
                                },new Label
                                {
                                    BackgroundColor=Color.FromHex("#e3e3e3"),
                                    HeightRequest=1
                                },new Label
                                {
                                    Text = "&#169; 2018. All Rights Reserved.",
                                    BackgroundColor=Color.FromHex("#e3e3e3"),
                                    VerticalOptions=LayoutOptions.FillAndExpand,
                                    VerticalTextAlignment=TextAlignment.Center,
                                    HorizontalTextAlignment=TextAlignment.Center
                                }
                            }
                        }
                    }
                }
            };

            return contentPage;
        }
    }
}
