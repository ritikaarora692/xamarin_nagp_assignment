﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoList.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ToDoList.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        LoginViewModel vm;
        public LoginPage()
        {
            vm = new LoginViewModel(Navigation,this);
            this.BindingContext = vm;
            vm.DisplayInvalidLoginPrompt += () => DisplayAlert("Incorrect Credentials", "Username/Password is Incorrect.", "OK");
            InitializeComponent();

            Email.Completed += (object sender, EventArgs e) =>
            {
             Password.Focus();
             };

            Password.Completed += (object sender, EventArgs e) =>
            {
             vm.SubmitCommand.Execute(null);
            };
        }

        protected override void OnAppearing()
        {
            this.BindingContext = vm;
        }

    }
}