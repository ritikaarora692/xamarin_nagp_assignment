﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoList.Model;
using ToDoList.services;
using ToDoList.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ToDoList.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        FacebookViewModel vm;
        string token;

        public ProfilePage(string msg)
        {
            token = msg;
            InitializeComponent();

            Application.Current.Properties["key"] = msg;

            vm = BindingContext as FacebookViewModel;

            getData();
            MessagingCenter.Subscribe<byte[]>(this, "ImageSelected", (args) =>
            {
                Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
                {
                    //Set the source of the image view with the byte array
                    img.Source = ImageSource.FromStream(() => new MemoryStream((byte[])args));
                });
            });

        }



        async void getData()
        {
            await vm.SetFacebookUserProfileAsync(token);

            Content = MainStackLayout;
        }

        public void NavigateToQuiz()
        {
            Navigation.PushModalAsync(new MyQuiz());
        }

        protected override bool OnBackButtonPressed()
        {
            Xamarin.Forms.Device.BeginInvokeOnMainThread(async () => {
                var result = await this.DisplayAlert("Alert!", "Do you really want to exit?", "Yes", "No");
                if (result)
                {
                    if (Xamarin.Forms.Device.OS == TargetPlatform.Android)
                    {
                        DependencyService.Get<IAndroidMethods>().CloseApp();
                    }
                    else
                    {
                        await this.Navigation.PopModalAsync();

                    }
                } // or anything else
            });

            return true;
        }

        public async void SelectImageClicked(object sender, EventArgs args)
        {
            Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
                {
                    var fileName = SetImageFileName();
                    DependencyService.Get<ISelectImage>().LaunchGallery(FileFormats.JPEG, fileName);
                });
            
        }

        private string SetImageFileName(string fileName = null)
        {
            if (fileName != null)
                    App.ImageIdToSave = fileName;
                else
                    App.ImageIdToSave = App.DefaultImageId;

                return App.ImageIdToSave;
           }
      
    }

     
    }
