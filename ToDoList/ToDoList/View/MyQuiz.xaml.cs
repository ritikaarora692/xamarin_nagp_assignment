﻿using CrossPieCharts.FormsPlugin.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoList.Model;
using ToDoList.services;
using ToDoList.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ToDoList.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyQuiz : ContentPage
    {
        int points = 20;
        INavigation _navigation;


        public MyQuiz()
        {
            InitializeComponent();

            this.BindingContext = new QuestionViewModel();

            ((QuestionViewModel)BindingContext).LoadQuestions();

            optionOne.Clicked += (sender, ea) =>
            {
                if (((QuestionViewModel)BindingContext).CheckIfCorrect(1)) DoAnswer();
                else
                {
                    DoWrongAnswer();
                }
            };

            optionTwo.Clicked += (sender, ea) =>
            {
                if (((QuestionViewModel)BindingContext).CheckIfCorrect(2)) DoAnswer();
                else
                {
                    DoWrongAnswer();
                }
            };

            optionThree.Clicked += (sender, ea) =>
            {
                if (((QuestionViewModel)BindingContext).CheckIfCorrect(3))
                {
                    DoAnswer();
                }
                else
                {
                    DoWrongAnswer();
                }
            };
        }

        private void DoAnswer()
        {
            App.Points += points;
            if (App.CurrentQuest < App.TOTAL_QUESTION)
            {
                App.CurrentQuest++;
                ((QuestionViewModel)BindingContext).ChooseNewQuestion();
            }
            else
            {
                NavigateToEndPage();
            }
        }

        private void DoWrongAnswer()
        {

            if (App.CurrentQuest < App.TOTAL_QUESTION)
            {
                App.CurrentQuest++;
                ((QuestionViewModel)BindingContext).ChooseNewQuestion();
            }
            else
            {
                NavigateToEndPage();
            }
        }

        private async void NavigateToEndPage()
        {
            await Navigation.PushModalAsync(new CrossPieChartSample().GetPageWithPieChart(App.Points, App.Username));
            insertUser();
        }

        public async void insertUser()
        {
            QuestionService _questionService = new QuestionService();
            UserPoints item = new UserPoints()
            {
                Username = App.Username,
                Score = App.Points
            };

            _questionService.InsertPoints(item);
        }
    }
}