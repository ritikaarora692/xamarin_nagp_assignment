﻿
using Android.App;
using Android.Content;
using ToDoList.Droid.Implementaions;
using ToDoList.Model;
using ToDoList.services;
using Xamarin.Forms;

[assembly: Dependency(typeof(AndroidSelectImage))]
namespace ToDoList.Droid.Implementaions
{
    class AndroidSelectImage : ISelectImage
    {
        public void LaunchGallery(FileFormats fileType, string imageId)
        {
            var imageIntent = new Intent();
            imageIntent.SetType("image/*");

            if (imageId != null)
            {
                imageIntent.PutExtra("fileName", imageId + "." + fileType.ToString());
            }
            imageIntent.SetAction(Intent.ActionGetContent);

            ((Activity)MainActivity.ActivityContext).StartActivityForResult(Intent.CreateChooser(imageIntent, "Select photo"), 1);
        }
    }
    }
