package md55967ce1b21854f024eaf9545d1a5604c;


public class FacebookLoginButtonRenderer
	extends md51558244f76c53b6aeda52c8a337f2c37.ViewRenderer
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("ToDoList.Droid.Renderers.FacebookLoginButtonRenderer, ToDoList.Android", FacebookLoginButtonRenderer.class, __md_methods);
	}


	public FacebookLoginButtonRenderer (android.content.Context p0)
	{
		super (p0);
		if (getClass () == FacebookLoginButtonRenderer.class)
			mono.android.TypeManager.Activate ("ToDoList.Droid.Renderers.FacebookLoginButtonRenderer, ToDoList.Android", "Android.Content.Context, Mono.Android", this, new java.lang.Object[] { p0 });
	}


	public FacebookLoginButtonRenderer (android.content.Context p0, android.util.AttributeSet p1, int p2)
	{
		super (p0, p1, p2);
		if (getClass () == FacebookLoginButtonRenderer.class)
			mono.android.TypeManager.Activate ("ToDoList.Droid.Renderers.FacebookLoginButtonRenderer, ToDoList.Android", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android:System.Int32, mscorlib", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public FacebookLoginButtonRenderer (android.content.Context p0, android.util.AttributeSet p1)
	{
		super (p0, p1);
		if (getClass () == FacebookLoginButtonRenderer.class)
			mono.android.TypeManager.Activate ("ToDoList.Droid.Renderers.FacebookLoginButtonRenderer, ToDoList.Android", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android", this, new java.lang.Object[] { p0, p1 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
